#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void contadorArreglo();

int main(int argc, char * argv[]){
	srand(time(NULL));
	
	
	contadorArreglo();
}

void contadorArreglo(){
	clock_t inicio, fin;
	double tiempoCPU;
	int i,random;
	inicio = clock();
	int *arreglo = malloc(sizeof(int)*536870912);
	//int arreglo[536870912];
	for(i=0;i<536870912;i++){
		random = rand()%536870912;
		arreglo[random]= random;
	}
	fin = clock();
	tiempoCPU = ((double)(fin-inicio))/CLOCKS_PER_SEC;
	printf("Tiempo de ejecucion: %f\n",tiempoCPU);
}
