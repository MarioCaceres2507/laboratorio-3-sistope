NOMBRE_EJECUTABLE1 = programa1
NOMBRE_EJECUTABLE2 = programa2
# Esta es la target que se ejecuta por defecto si se escribe "make" en la consola
all: release

# Compilación principal del ejecutable
release:
	@echo 'Compilando target: $@'
	gcc Secuencial.c -o $(NOMBRE_EJECUTABLE1)
	gcc Aleatorio.c -o $(NOMBRE_EJECUTABLE2)
	@echo ' '
	@echo "Ejecute el programa de acceso secuencial haciendo: ./"$(NOMBRE_EJECUTABLE1)
	@echo "Ejecute el programa de acceso aleatorio haciendo: ./"$(NOMBRE_EJECUTABLE2)
# Other Targets
clean:
	#Borro el contenido del directorio de compilación
	rm -rf $(DIR_OUTPUT)/*
	-@echo ' ' 
